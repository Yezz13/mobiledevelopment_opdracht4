import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage { 

  memeData = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage) {

    this.getStoredData();

  }

  public getStoredData() {
    this.storage.get('jsonData').then((val) => {
      if(val!=null)
      {
      this.memeData = val;
      console.log(this.memeData);
      }
    });
  }
}
