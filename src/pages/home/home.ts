import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { Storage } from '@ionic/storage';
import { AlertController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  memeData = [];

  constructor(public navCtrl: NavController, private storage: Storage, private alertCtrl: AlertController) {
    this.getStoredData();
  }
  
  getStoredData() {
    this.storage.get('jsonData').then((val) => {
      this.memeData = val;
    });
  } 

  deleteData () {
    let alert = this.alertCtrl.create({
      title: 'Confirm purchase',
      message: 'Are you sure you want to delete all the memes? :\'(',
      buttons: [
        {
          text: 'No, go back!',
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: 'Yes, delete!',
          handler: () => {
            this.memeData = [];
            this.storage.set('jsonData', this.memeData);

            let alert = this.alertCtrl.create({
              title: 'Succesful wipe!',
              subTitle: 'All memes have been deleted! Article 13 will doom us all...',
              buttons: ['Too bad!']
            });
            alert.present();
          }
        }
      ]
    });
    alert.present();
  }
}
