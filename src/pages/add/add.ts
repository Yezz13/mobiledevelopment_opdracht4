import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams} from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Storage } from '@ionic/storage';
import { AlertController } from 'ionic-angular';

/**
 * Generated class for the AddPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add',
  templateUrl: 'add.html',
})
export class AddPage {

  name: string;
  description: string;

  Meme:any ={
    name: "",
    img: "",
    description:""
  };

  image: any;

  memeData = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, private camera: Camera, private storage: Storage, private alertCtrl: AlertController) {

    this.getStoredData();
  }

  
  addMeme(){
    if (this.name != null || this.description != null) {
    this.Meme.name = this.name;
    this.Meme.description = this.description;
    this.Meme.img = this.image;

    this.memeData.push(this.Meme);

    this.storage.set('jsonData', this.memeData);

    this.presentAlert();
    }
    else{
      this.presentWarning();
    }
  }

  selectImage () {
    const options: CameraOptions = {
      quality: 50,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
    }

    this.camera.getPicture(options).then((imageData) => {
      this.image = 'data:image/jpeg;base64,' + imageData;
     });
  }

  public getStoredData() {
    this.storage.get('jsonData').then((val) => {
      this.memeData = val;
    });
  }

  presentAlert() {
    let alert = this.alertCtrl.create({
      title: 'Succesful Upload!',
      subTitle: 'Your meme has been added to Meme-o-pedia!',
      buttons: ['Great!']
    });
    alert.present();
  }

  presentWarning() {
    let alert = this.alertCtrl.create({
      title: 'Warning',
      subTitle: 'Please fill in all the fields!',
      buttons: ['Oops!']
    });
    alert.present();
  }
}
